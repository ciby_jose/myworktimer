//
//  AppDelegate.h
//  MyWorkHoursTimer
//
//  Created by developer on 18/07/16.
//  Copyright © 2016 TheBigMove. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

