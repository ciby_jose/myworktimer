//
//  ViewController.m
//  MyWorkHoursTimer
//
//  Created by developer on 18/07/16.
//  Copyright © 2016 TheBigMove. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <DateTools.h>

static double const Klongitude = 76.361633;
static double const Klatitude =  10.009253;

@interface ViewController ()<CLLocationManagerDelegate>

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,weak) IBOutlet UIButton *startTimerButton;
@property (nonatomic,weak) IBOutlet UILabel *latitiudeLabel;
@property (nonatomic,weak) IBOutlet UILabel *longitude;
@property (nonatomic,weak) IBOutlet UILabel *TimerLabel;
@property (nonatomic,weak) IBOutlet UILabel *workedHoursLabel;
@property (nonatomic,weak) IBOutlet UILabel *RequireHoursLabel;
@property (nonatomic,assign) BOOL isTimerRunning;
@property(assign,nonatomic)NSTimeInterval timeInterval;
@property(assign,nonatomic)NSTimeInterval workedtimeInterval;

@property (nonatomic,strong) NSTimer *stopTimer;
@property (nonatomic,strong) NSDate *targetTime;
@property (nonatomic,strong) NSDate *InTime;



@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isTimerRunning = false;
    
    self.locationManager = [[CLLocationManager alloc]init]; // initializing locationManager
    self.locationManager.delegate = self; // we set the delegate of locationManager to self.
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest; // setting the accuracy
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
    
    NSDate *CurrentDate = [NSDate date];
    NSDate *morningAlarm  = [NSDate dateWithYear:[CurrentDate year]  month:[CurrentDate month]  day: [CurrentDate day] hour:9 minute:0 second:0];
    [self updateNotificationWithTime:morningAlarm WithMessage:@"Good morning.Its time too start the day" andNotificationType:@"Morning"];
    
    [self.TimerLabel clipsToBounds];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    self.targetTime = [preferences objectForKey:@"targetTime"];
    self.InTime = [preferences objectForKey:@"inTime"];
    
    double  totalTimeInterval = [preferences doubleForKey:@"totalTime"];
    NSNumber *time = [NSNumber numberWithDouble:totalTimeInterval];
    NSTimeInterval totalTime = [time doubleValue];
    long seconds = lroundf(totalTime);
    
    NSString *string = [NSString stringWithFormat:@"%02li:%02li:%02li",
                        lround(floor(seconds / 3600.)),
                        lround(floor(seconds % 3600)) / 60,
                        lround(floor(seconds)) % 60];
    
    self.workedHoursLabel.text =string;
    
    
    if (self.targetTime){
        self.latitiudeLabel.text = [self ConvertTimeToString:self.InTime];
        self.longitude.text = [self ConvertTimeToString:self.targetTime];
        self.timeInterval = [self.targetTime timeIntervalSinceDate:[NSDate date]];
        if (self.stopTimer == nil) {
            self.stopTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                              target:self
                                                            selector:@selector(updateTimer)
                                                            userInfo:nil
                                                             repeats:YES];
        }
    }
    
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (!self.isTimerRunning){
        CLLocation *crnLoc = [locations lastObject];
        CLLocation *location = [[CLLocation alloc]initWithLatitude:Klatitude longitude:Klongitude];
        CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:crnLoc.coordinate.latitude longitude:crnLoc.coordinate.longitude];
        
        
        CLLocationDistance distance = [location distanceFromLocation:currentLocation];
        if (distance  < 10) {
            [self StartTimer:nil];
            [manager stopUpdatingLocation];
        }
    }
}




-(NSString*)ConvertTimeToString:(NSDate*)timeToBeConverted{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *timeString=[dateFormatter stringFromDate:timeToBeConverted];
    return timeString;
    
}

-(void)updateTimer  {
    
    self.timeInterval--;
    long seconds = lroundf( self.timeInterval);
    
    NSString *string = [NSString stringWithFormat:@"%02li:%02li:%02li",
                        lround(floor(seconds / 3600.)),
                        lround(floor(seconds % 3600)) / 60,
                        lround(floor(self.timeInterval)) % 60];
    
    
    // int seconds = (self.timeInterval %3600) % 60;
    self.TimerLabel.text =string;
    [self updateNotificationWithTime:self.targetTime WithMessage:@"Time to leave" andNotificationType:@"Evening"];
}

-(void)saveTargetTime {
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:self.targetTime forKey:@"targetTime"];
    [preferences setObject:self.InTime forKey:@"inTime"];
    
}

-(void)updateNotificationWithTime:(NSDate*)alarmtime WithMessage:(NSString *)messsage
              andNotificationType:(NSString*)type {
    
    UIApplication *app =[UIApplication sharedApplication];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = alarmtime;
    NSTimeZone* timezone = [NSTimeZone defaultTimeZone];
    notification.timeZone = timezone;
    notification.alertBody = messsage;
    notification.alertAction = @"Show";
    NSDictionary *infoDict = @{@"NotifType" : type};
    notification.userInfo = infoDict;
    app.applicationIconBadgeNumber=1;
    notification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
}


-(IBAction)stopTomer:(id)sender{
    [self.stopTimer invalidate];
    self.isTimerRunning= false;
    [self.startTimerButton setEnabled:YES];
    [self.startTimerButton setUserInteractionEnabled:YES];
    [self clearpreferneces];
    self.stopTimer =nil;
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    double  totalTimeInterval = [preferences doubleForKey:@"totalTime"];
    self.workedtimeInterval = [[NSDate date] timeIntervalSinceDate:self.InTime ];
    
    NSNumber *time = [NSNumber numberWithDouble:totalTimeInterval];
    NSTimeInterval totalTime = [time doubleValue];
    totalTime = totalTime + self.workedtimeInterval;
    [preferences setDouble:totalTime forKey:@"totalTime"];
    long seconds = lroundf(totalTime);
    
    NSString *string = [NSString stringWithFormat:@"%02li:%02li:%02li",
                        lround(floor(seconds / 3600.)),
                        lround(floor(seconds % 3600)) / 60,
                        lround(floor(seconds)) % 60];
    
    self.workedHoursLabel.text =string;
}
-(void)stopTimeFromNotifcation{
    [self.stopTimer invalidate];
    self.isTimerRunning= false;
    [self.startTimerButton setEnabled:YES];
    [self.startTimerButton setUserInteractionEnabled:YES];
    [self clearpreferneces];
    self.stopTimer =nil;
}


-(IBAction)setTargetLocation:(id)sender{
    
}

-(IBAction)setTargetTimer:(id)sender{
    
}

-(void) clearpreferneces{
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:nil forKey:@"targetTime"];
    [preferences setObject:nil forKey:@"inTime"];
    
    
}

-(IBAction)StartTimer:(id)sender{
    if (!self.isTimerRunning) {
        
        [self.startTimerButton setEnabled:NO];
        [self.startTimerButton setUserInteractionEnabled:NO];
        
        [self clearpreferneces];
        
        self.isTimerRunning= true;
        self.InTime = [NSDate date];
        self.targetTime= [self.InTime dateByAddingHours:9];
        [self saveTargetTime];
        self.latitiudeLabel.text = [self ConvertTimeToString:self.InTime];
        self.longitude.text = [self ConvertTimeToString:self.targetTime];
        
        self.timeInterval = [self.targetTime timeIntervalSinceDate:self.InTime];
        
        if (self.stopTimer == nil) {
            self.stopTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                              target:self
                                                            selector:@selector(updateTimer)
                                                            userInfo:nil
                                                             repeats:YES];
            
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
