//
//  UIView+CustomViewProperty.h
//  NextEraEnergy
//
//  Created by developer on 10/06/16.
//  Copyright © 2016 FPL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CustomViewProperty)

@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable NSMutableDictionary* jaba;


@end
