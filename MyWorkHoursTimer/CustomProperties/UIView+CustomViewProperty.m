//
//  UIView+CustomViewProperty.m
//  NextEraEnergy
//
//  Created by developer on 10/06/16.
//  Copyright © 2016 FPL. All rights reserved.
//

#import "UIView+CustomViewProperty.h"

@implementation UIView (CustomViewProperty)
@dynamic borderColor,borderWidth,cornerRadius,jaba;

-(void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

-(void)setBorderWidth:(CGFloat)borderWidth{
    [self.layer setBorderWidth:borderWidth];
}

-(void)setBorderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth{
    [self.layer setBorderColor:borderColor.CGColor];
    [self.layer setBorderWidth:borderWidth];
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    [self.layer setCornerRadius:cornerRadius];
    [self.layer setMasksToBounds:YES];
}

@end
